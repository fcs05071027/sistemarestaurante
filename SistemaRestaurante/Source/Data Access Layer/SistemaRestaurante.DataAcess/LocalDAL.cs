﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SistemaRestaurante.Utils;

namespace SistemaRestaurante.DataAcess
{
    using System.Data;

    using Microsoft.Practices.EnterpriseLibrary.Data;

    using SistemaRestaurante.BusinessEntity;

    public class LocalDAL : Singleton<LocalDAL>
    {
        private readonly Database BaseDatos = DatabaseFactory.CreateDatabase();

        public IList<Local> Listar()
        {
            var locales = new List<Local>();
            var comando = BaseDatos.GetStoredProcCommand("usp_SelectAll_Local");

            using (var lector = BaseDatos.ExecuteReader(comando))
            {
                while (lector.Read())
                {
                    locales.Add(new Local
                    {
                        IdLocal = lector.GetInt32(lector.GetOrdinal("IdLocal")),
                        Nombre = lector.GetString(lector.GetOrdinal("Nombre")),
                        Distrito = lector.GetString(lector.GetOrdinal("Distrito")),
                        Direccion = lector.IsDBNull(lector.GetOrdinal("Direccion")) ? string.Empty : lector.GetString(lector.GetOrdinal("Direccion")),
                        Telefono = lector.IsDBNull(lector.GetOrdinal("Telefono"))  ? string.Empty : lector.GetString(lector.GetOrdinal("Telefono"))
                    });
                }
            }
            comando.Dispose();
            return locales;
        }
        
        public IList<Local> Listar(string sordColumn, string sordDirection, int rowsCount, int page, int count, string where)
        {
            var locales = new List<Local>();
            var comando = BaseDatos.GetStoredProcCommand("usp_Listar_Locales_Paginado");
            BaseDatos.AddInParameter(comando, "SordColumn", DbType.String, sordColumn);
            BaseDatos.AddInParameter(comando, "SordDirection", DbType.String, sordDirection);
            BaseDatos.AddInParameter(comando, "RowsCount", DbType.Int32, rowsCount);
            BaseDatos.AddInParameter(comando, "Page", DbType.Int32, page);
            BaseDatos.AddInParameter(comando, "Count", DbType.Int32, count);
            BaseDatos.AddInParameter(comando, "Where", DbType.String, where);

            using (var lector = BaseDatos.ExecuteReader(comando))
            {
                while (lector.Read())
                {
                    locales.Add(new Local
                    {
                        IdLocal = lector.GetInt32(lector.GetOrdinal("IdLocal")),
                        Nombre = lector.GetString(lector.GetOrdinal("Nombre")),
                        Distrito = lector.GetString(lector.GetOrdinal("Distrito")),
                        Direccion = lector.IsDBNull(lector.GetOrdinal("Direccion")) ? string.Empty : lector.GetString(lector.GetOrdinal("Direccion")),
                        Telefono = lector.IsDBNull(lector.GetOrdinal("Telefono"))  ? string.Empty : lector.GetString(lector.GetOrdinal("Telefono"))
                    });
                }
            }
            comando.Dispose();
            return locales;
        }
       
        
        
        public Local Obtener(int idLocal)
        {
            var local = new Local();
            var comando = BaseDatos.GetStoredProcCommand("usp_Select_Local");
            BaseDatos.AddInParameter(comando, "IdLocal", DbType.Int32, idLocal);
            using (var lector = BaseDatos.ExecuteReader(comando))
            {
                if (lector.Read())
                {
                    local.IdLocal = lector.GetInt32(lector.GetOrdinal("IdLocal"));
                    local.Nombre = lector.GetString(lector.GetOrdinal("Nombre"));
                    local.Distrito = lector.GetString(lector.GetOrdinal("Distrito"));
                    local.Direccion = lector.IsDBNull(lector.GetOrdinal("Direccion"))
                                          ? string.Empty
                                          : lector.GetString(lector.GetOrdinal("Direccion"));
                    local.Telefono = lector.IsDBNull(lector.GetOrdinal("Telefono"))
                                         ? string.Empty
                                         : lector.GetString(lector.GetOrdinal("Telefono"));
                }
            }
            comando.Dispose();
            return local;
        }

        public int Agregar(Local local)
        {
            var comando = BaseDatos.GetStoredProcCommand("usp_Insert_Local");
            BaseDatos.AddOutParameter(comando, "IdLocal", DbType.Int32, 4);
            BaseDatos.AddInParameter(comando, "Nombre", DbType.String, local.Nombre);
            BaseDatos.AddInParameter(comando, "Distrito", DbType.String, local.Distrito);
            BaseDatos.AddInParameter(comando,"Direccion", DbType.String,local.Direccion);
            BaseDatos.AddInParameter(comando,"Telefono",DbType.String,local.Telefono);

            var resultado = BaseDatos.ExecuteNonQuery(comando);
            if (resultado == 0) throw new Exception("No se pudo insertar el registro en la base de datos");

            var valor = (int)BaseDatos.GetParameterValue(comando, "IdLocal");
            comando.Dispose();

            return valor;
        }

        public bool Modificar(Local local)
        {
            var comando = BaseDatos.GetStoredProcCommand("usp_Update_Local");
            BaseDatos.AddInParameter(comando, "IdLocal", DbType.Int32, local.IdLocal);
            BaseDatos.AddInParameter(comando, "Nombre", DbType.String, local.Nombre);
            BaseDatos.AddInParameter(comando, "Direccion", DbType.String, local.Direccion);
            BaseDatos.AddInParameter(comando, "Distrito", DbType.String, local.Distrito);
            BaseDatos.AddInParameter(comando, "Telefono", DbType.String, local.Telefono);

            var resultado = BaseDatos.ExecuteNonQuery(comando);
            if (resultado == 0) throw new Exception("Hubo un error al modificar");

            comando.Dispose();

            return true;

        }

        public bool Eliminar(int idLocal)
        {
            var comando = BaseDatos.GetStoredProcCommand("usp_Delete_Local");
            BaseDatos.AddInParameter(comando, "IdLocal", DbType.Int32, idLocal);

            var resultado = BaseDatos.ExecuteNonQuery(comando);
            if (resultado == 0) throw new Exception("No se pudo eliminar el registro");

            comando.Dispose();

            return true;
        }

        public int Count(string where)
        {
            var cantidad = 0;
            var comando = BaseDatos.GetStoredProcCommand("usp_Count_Local");
            BaseDatos.AddInParameter(comando, "Where", DbType.String, where);
            using (var dr = BaseDatos.ExecuteReader(comando))
            {
                if(dr.Read())
                {
                    cantidad = dr.GetInt32(dr.GetOrdinal("Cantidad"));
                }
            }


            comando.Dispose();
            return cantidad;
        }



    }
}
