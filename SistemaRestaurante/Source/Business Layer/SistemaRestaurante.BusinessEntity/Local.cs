﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel.DataAnnotations;

namespace SistemaRestaurante.BusinessEntity
{
    public class Local
    {
        public int IdLocal { get; set; }
        
        [Required]
        public string  Nombre { get; set; }

        public string Direccion { get; set; }
        public string Telefono { get; set; }

        [Required]
        public string Distrito { get; set; }

        public string Provincia { set; get; }

    }
}
