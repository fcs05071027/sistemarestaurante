﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using SistemaRestaurante.BusinessEntity;
using SistemaRestaurante.DataAcess;
using SistemaRestaurante.Utils;

namespace SistemaRestaurante.BusinessLogic
{
    public class UsuarioBL : Singleton<UsuarioBL>
    {
        public Usuario Obtener(string username, string password)
        {
            try
            {
                return UsuarioDAL.Instancia.Obtener(username, password);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
